<?php


namespace lingyaoworld\jwt;

use lingyaoworld\jwt\command\SecretCommand;
use lingyaoworld\jwt\middleware\InjectJwt;
use lingyaoworld\jwt\provider\JWT as JWTProvider;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(SecretCommand::class);
        $this->app->middleware->add(InjectJwt::class);
    }
}
