<?php


namespace lingyaoworld\jwt\provider\storage;


use lingyaoworld\jwt\contract\Storage;
use think\facade\Cache;

class Tp8 implements Storage
{
    public function delete($key)
    {
        return Cache::delete($key);
    }

    public function get($key)
    {
        return Cache::get($key);
    }

    public function set($key, $val, $time = 0)
    {
        return Cache::set($key, $val, $time);
    }
}